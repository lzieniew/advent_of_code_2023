def process_part_one(input_text):
    output = []

    for line in input_text.split("\n"):
        onlydigits = "".join([char for char in line if char.isdigit()])
        output.append(int(onlydigits[0] + onlydigits[-1]))

    return sum(output)


def process_part_two(input_text):
    return input_text
