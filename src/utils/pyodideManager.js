let pyodideLoadingPromise = null;
let pyodideInstance = null;

export function loadPyodide() {
  if (pyodideInstance) {
    return Promise.resolve(pyodideInstance);
  }

  if (!pyodideLoadingPromise) {
    pyodideLoadingPromise = new Promise((resolve, reject) => {
      const script = document.createElement('script');
      script.src = "https://cdn.jsdelivr.net/pyodide/v0.18.1/full/pyodide.js";
      script.onload = () => {
        window.loadPyodide({ indexURL: "https://cdn.jsdelivr.net/pyodide/v0.18.1/full/" })
          .then((loadedPyodide) => {
            pyodideInstance = loadedPyodide;
            resolve(pyodideInstance);
          })
          .catch(reject);
      };
      script.onerror = reject;
      document.head.appendChild(script);
    });
  }

  return pyodideLoadingPromise;
}

